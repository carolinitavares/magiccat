//
//  GameScene.swift
//  MagicCat
//
//  Created by Carolini Freire Ardito Tavares on 2019-03-04.
//  Copyright © 2019 Carolini Freire Ardito Tavares. All rights reserved.
//

import SpriteKit
import GameplayKit

struct CollisionCategories{
    static let Cat : UInt32 = 0x1 << 0
    static let Player: UInt32 = 0x1 << 1
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    
    var backgroundMusic:SKAudioNode = SKAudioNode()
    
    var player:SKSpriteNode = SKSpriteNode()
    var cat1:SKSpriteNode = SKSpriteNode()
    var cat2:SKSpriteNode = SKSpriteNode()
    var Level2Cat1:SKSpriteNode = SKSpriteNode()
    var Level2Cat2:SKSpriteNode = SKSpriteNode()
    
    var scoreScreen:SKLabelNode = SKLabelNode()
    var livesScreen:SKLabelNode = SKLabelNode()
    
    var musicOn = true
    var musicBtn: SKSpriteNode = SKSpriteNode()
    
    // MARK: Scoring and Lives variables
    var score = 0
    var lives = 3
    
    // MARK: VARAIBLES TO TRACK GAME STATE
    var levelComplete = false
    var currentLevel = 1
    //var orange:Orange?
    var mouseStartingPosition:CGPoint = CGPoint(x:0, y:0)
    var lineCat:SKShapeNode = SKShapeNode()
    var line1Cat:SKShapeNode = SKShapeNode()
    var line2Cat:SKShapeNode = SKShapeNode()
    
    var angle1 = 0.0
    var moveToLine1X = 580.0
    var moveToLine1Y = 0.0
    var addLine1X = 500.0
    var addLine1Y = 0.0
    
    var angle2 = 90.0
    var moveToLine2X = -580.0
    var moveToLine2Y = 80.0
    var addLine2X = -580.0
    var addLine2Y = 0.0
    
    var randomXCat1 = 0
    var randomYCat1 = 0
    var randomXCat2 = 0
    var randomYCat2 = 0

    var hit = false
    var goingLeft = false

    
    override func didMove(to view: SKView) {
        
        self.physicsWorld.contactDelegate = self
        
        //Music
        if let musicURL = Bundle.main.url(forResource: "main-theme02", withExtension: "m4a") {
            backgroundMusic = SKAudioNode(url: musicURL)
            addChild(backgroundMusic)
        }
        
        self.player = self.childNode(withName: "player") as! SKSpriteNode
        self.cat1 = self.childNode(withName: "cat1") as! SKSpriteNode
        self.cat2 = self.childNode(withName: "cat2") as! SKSpriteNode
        self.Level2Cat1 = self.childNode(withName: "cat1") as! SKSpriteNode
        self.Level2Cat2 = self.childNode(withName: "cat2") as! SKSpriteNode
        
        self.scoreScreen = self.childNode(withName: "score") as! SKLabelNode
        self.scoreScreen.text = "\(self.score)"
        self.livesScreen = self.childNode(withName: "lives") as! SKLabelNode
        self.livesScreen.text = "\(self.lives)"
        
        self.musicBtn = self.childNode(withName: "musicButton") as! SKSpriteNode
        
        self.player.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.player.frame.width, height: self.player.frame.height))
        self.player.physicsBody?.affectedByGravity = false
        self.player.physicsBody?.isDynamic = true
        self.player.physicsBody?.categoryBitMask = CollisionCategories.Player
        //self.player.physicsBody?.collisionBitMask = CollisionCategories.Enemy + CollisionCategories.otto
        //self.player.physicsBody?.collisionBitMask = CollisionCategories.Enemy + CollisionCategories.otto
        
        self.cat1.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.cat1.frame.width, height: self.cat1.frame.height))
        self.cat1.physicsBody?.affectedByGravity = false
        self.cat1.physicsBody?.isDynamic = true
        self.cat1.physicsBody?.categoryBitMask = CollisionCategories.Cat
        
        self.cat2.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.cat2.frame.width, height: self.cat2.frame.height))
        self.cat2.physicsBody?.affectedByGravity = false
        self.cat2.physicsBody?.isDynamic = true
        self.cat2.physicsBody?.categoryBitMask = CollisionCategories.Cat
        self.cat2.xScale = self.cat2.xScale * -1;
        
        // set values labels
        self.scoreScreen.text = "\(self.score)"
        self.livesScreen.text = "\(self.lives)"
        
        if(levelComplete == true) {
            
            randomCat1()
            randomCat2()
        } else {
            //configure drawing line
            self.lineCat.lineWidth = 10
            self.lineCat.strokeColor  = .black
            addChild(lineCat)
            
            line1()
            line2()
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        print("entrei")
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        print("first = \(firstBody.node?.name)")
        print("second = \(secondBody.node?.name)")
        
        
        if (firstBody.node?.name == "cat1" && secondBody.node?.name == "player") {
            firstBody.node?.removeAllChildren()
            firstBody.node?.removeFromParent()
            print("cat 1")
        }
        if (firstBody.node?.name == "cat2" && secondBody.node?.name == "player") {
            firstBody.node?.removeAllChildren()
            firstBody.node?.removeFromParent()
            print("cat 2")
        }
        
        
    }
    
    func gameOver(){
        let message = SKLabelNode(text:"GAME OVER")
        message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        message.fontColor = UIColor.red
        message.fontSize = 100
        message.fontName = "AvenirNext-Bold"
        
        addChild(message)
        
        
        //RESTART
        // load the next level
        self.currentLevel = 1
        
        // try to load the next level
        guard let nextLevelScene = GameScene.loadLevel(levelNumber: self.currentLevel) else {
            print("Error when loading next level")
            return
            
        }
        
        // wait 2 second then switch to next leevl
        let waitAction = SKAction.wait(forDuration:2)
        
        let showNextLevelAction = SKAction.run {
            self.score = 0
            self.scoreScreen.text = "\(self.score)"
            self.lives = 2
            self.livesScreen.text = "\(self.lives)"
            nextLevelScene.setLevel(levelNumber: self.currentLevel)
            let transition = SKTransition.flipVertical(withDuration: 2)
            nextLevelScene.scaleMode = .aspectFill
            self.scene?.view?.presentScene(nextLevelScene, transition:transition)
        }
        
        let sequence = SKAction.sequence([waitAction, showNextLevelAction])
        
        self.run(sequence)
    }
    
    func gameWin() {
        self.levelComplete = true
        
        let message = SKLabelNode(text:"LEVEL \(self.currentLevel) COMPLETE!")
        message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        message.fontColor = UIColor.magenta
        message.fontSize = 100
        message.fontName = "AvenirNext-Bold"
        
        addChild(message)
        
        // load the next level
        self.currentLevel = self.currentLevel + 1
        
        // try to load the next level
        guard let nextLevelScene = GameScene.loadLevel(levelNumber: self.currentLevel) else {
            print("Error when loading next level")
            return
            
        }
        
        // wait 1 second then switch to next leevl
        let waitAction = SKAction.wait(forDuration:1)
        
        let showNextLevelAction = SKAction.run {
            self.score = self.score + 50
            self.scoreScreen.text = "\(self.score)"
            nextLevelScene.setLevel(levelNumber: self.currentLevel)
            let transition = SKTransition.flipVertical(withDuration: 2)
            nextLevelScene.scaleMode = .aspectFill
            self.scene?.view?.presentScene(nextLevelScene, transition:transition)
        }
        
        let sequence = SKAction.sequence([waitAction, showNextLevelAction])
        
        self.run(sequence)
    }
    
    func setLevel(levelNumber: Int){
        self.currentLevel = levelNumber
    }
    
    class func loadLevel(levelNumber:Int) -> GameScene? {
        let fileName = "GameScene\(levelNumber)"
        
        // DEBUG nonsense
        print("Trying to load file: \(fileName).sks")
        
        guard let scene = GameScene(fileNamed: fileName) else {
            print("Cannot find level named: \(fileName).sks")
            return nil
        }
        
        scene.scaleMode = .aspectFill
        return scene
        
    }
    
    func line1() {
        self.line1Cat.lineWidth = 10
        self.line1Cat.strokeColor = .orange
        addChild(line1Cat)
        
        let moveAction = SKAction.moveBy(x: CGFloat(-25), y: 0, duration: 2.5)
        let sequence:SKAction = SKAction.sequence([moveAction, moveAction])
        line1Cat.run(SKAction.repeatForever(sequence))
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: moveToLine1X, y: moveToLine1Y))
        path.addLine(to: CGPoint(x:addLine1X, y:addLine1Y))
        self.line1Cat.path = path.cgPath
    }
    
    func line2() {
        self.line2Cat.lineWidth = 10
        self.line2Cat.strokeColor = .brown
        addChild(line2Cat)
        
        let moveAction = SKAction.moveBy(x: (CGFloat(25)), y:0 , duration: 2.5)
        let sequence1:SKAction = SKAction.sequence([moveAction, moveAction])
        line2Cat.run(SKAction.repeatForever(sequence1))
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x:moveToLine2X , y: moveToLine2Y))
        path.addLine(to: CGPoint(x: addLine2X, y: addLine2Y))
        self.line2Cat.path = path.cgPath
    }
    
    func randomCat1() {
        randomXCat1 = Int(arc4random_uniform(UInt32(size.width)))
        randomYCat1 = Int(arc4random_uniform(UInt32(size.height)))
        Level2Cat1.position = CGPoint(x: CGFloat(randomXCat1), y: CGFloat(randomYCat1))
        
        //hitbox for randomCAT
        let bodysize = CGSize(width: Level2Cat1.size.width, height: Level2Cat1.size.height)
        Level2Cat1.physicsBody = SKPhysicsBody(rectangleOf: bodysize)
        Level2Cat1.physicsBody?.isDynamic = false
        
        addChild(Level2Cat1)
        let Level2Cat1Movement = SKAction.move(to: CGPoint(x:self.player.position.x, y: self.player.position.y), duration: 20)
        Level2Cat1.run(Level2Cat1Movement)
    }
    
    func randomCat2() {
        randomXCat2 = Int(arc4random_uniform(UInt32(size.width)))
        randomYCat2 = Int(arc4random_uniform(UInt32(size.height)))
        Level2Cat2.position = CGPoint(x: CGFloat(randomXCat2), y: CGFloat(randomYCat2))
        
        //hitbox for randomCAT
        let bodysize = CGSize(width: Level2Cat2.size.width, height: Level2Cat2.size.height)
        Level2Cat2.physicsBody = SKPhysicsBody(rectangleOf: bodysize)
        Level2Cat2.physicsBody?.isDynamic = false
        
        addChild(Level2Cat2)
        let Level2Cat2Movement = SKAction.move(to: CGPoint(x:self.player.position.x, y: self.player.position.y), duration: 20)
        Level2Cat2.run(Level2Cat2Movement)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let label = self.label {
            label.run(SKAction.init(named: "Pulse")!, withKey: "fadeInOut")
        }
        
        guard let touch = touches.first  else {
            return
        }
        
        let mouseLocation = touch.location(in: self)
        
        // detect what sprite was touched
        let spriteTouched = self.atPoint(mouseLocation)
        
        if (spriteTouched.name == "musicButton") {
            print("YOU CLICKED THE musica button")
            
            if (self.musicOn) {
                self.backgroundMusic.run(SKAction.stop())
                self.musicBtn.texture = SKTexture.init(imageNamed: "musicOff-light")
            }else{
                self.backgroundMusic.run(SKAction.play())
                self.musicBtn.texture = SKTexture.init(imageNamed: "musicOn-light")
            }
            self.musicOn = !self.musicOn
        }
        
        self.mouseStartingPosition = mouseLocation
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else{
            return
        }
        let mouseposition = touch.location(in: self)
        print("Finger Moved position: \(mouseposition)")
        
        //draw a line
        let path = UIBezierPath()
        path.move(to: self.mouseStartingPosition)
        path.addLine(to: mouseposition)
        self.lineCat.path = path.cgPath
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else{
            return
        }
        
        let mouseLocation = touch.location(in: self)
        let drawingEndingPosition = mouseLocation
        
        let diffX = drawingEndingPosition.x - self.mouseStartingPosition.x
        let diffY = drawingEndingPosition.y - self.mouseStartingPosition.y
        
        if(levelComplete == true) {
            
        } else if(levelComplete == false){
            if(diffX == 0.0 && diffY == 0.0){
                let direction = CGVector(dx: diffX, dy: diffY)
                return
            }
            else {
                var angle = atan2(diffY,diffX)
                var help = angle * 60
                
                let direction = CGVector(dx: diffX, dy: diffY)
                
                if((help >= 0.0 && help <= 15.0) ||
                    (help <= 0.0 && help >= -15.0)) {
                    help = 0
                }else if((help <= 190.0 && help >= 170.0) ||
                    (help >= -190.0 && help <= -170.0)){
                    help = 0
                }
                    
                else if(help >= 90 && help <= 100) ||
                    (help <= 90 && help >= 80){
                    help = 90
                }else if(help <= -90 && help >= -100) || (help >= -90 && help <= -80) {
                    help = 90
                }else {
                    print("nada")
                }
                
                self.lineCat.path = nil
                if(angle1 == Double(help)) {
                    self.line1Cat.path = nil
                    self.cat2.removeFromParent()
                    self.score = score + 50
                    self.scoreScreen.text = "\(score)"
                    currentLevel = currentLevel - 1
                }
                else if(angle2 == Double(help)) {
                    self.line2Cat.path = nil
                    self.cat1.removeFromParent()
                    self.score = score + 50
                    self.scoreScreen.text = "\(score)"
                    currentLevel = currentLevel - 1
                }
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
